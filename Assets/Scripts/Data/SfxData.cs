﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SfxData : MonoBehaviour
{
    public string id;
    public AudioClip audioClip;
}
