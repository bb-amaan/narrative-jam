﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BackgroundData : MonoBehaviour
{
    public string id;
    public Sprite image;
}
