﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneItem : MonoBehaviour
{
    public List<CoreScene> scenes = new List<CoreScene>();
    public string chapter;

    public void Init()
    {
        CoreScene[] coreScenes = GetComponentsInChildren<CoreScene>(); // load all the scenes

        foreach(CoreScene coreScene in coreScenes)
        {
            scenes.Add(coreScene);
        }

        Debug.LogFormat("SCENE: initalised {0} with {1} items", name, scenes.Count);
    }
}
