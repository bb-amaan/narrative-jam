﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EmotionData
{
    public Emotion emotion;
    public Sprite characterSprite;
}

// simplicity sake going to stick to gameobjects but could be swithced to scriptable object
public class Character : MonoBehaviour
{
    public CharacterName characterName;
    public Sprite mainCharacterShot;
    public List<EmotionData> emotions;

    //public int relationship = 0;

    public void UpdateRelationship(int amount)
    {
        string name = CharacterManager.GetName(characterName);
        string id = string.Format("{0}-{1}", name, Director.instance.sceneIndex);
        //relationship += amount;

        int current = PlayerPrefs.GetInt(id, 0);
        PlayerPrefs.SetInt(id, current + amount);

        Debug.LogFormat("Updated {0} relationship {1} new {2}", CharacterManager.GetName(characterName), amount, current + amount);
    }

    public Sprite GetEmotion(Emotion emotion)
    {
        EmotionData emotionData = emotions.Find(x => x.emotion == emotion);
        if (emotionData != null)
        {
            return emotionData.characterSprite;
        }

        Debug.LogWarning(string.Format("No emotion data found for emotion {0}", emotion));

        return mainCharacterShot;
    }

    public Sprite GetCharacterSprite()
    {
        return mainCharacterShot;
    }
}
