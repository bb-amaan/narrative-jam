﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum RoomState
{
    Day,
    Night
}

[System.Serializable]
public class ItemData
{
    public int stage;
    public List<Item> items;
}

public class Room : MonoBehaviour
{
    public string id;
    public Sprite dayBacking;
    public Sprite nightBacking;

    public List<ItemData> items;

    private List<Item> currentItems;
    public int itemCount;

    public void Init(RoomState state, int stage)
    {
        // disable all items
        for (int i = 0; i < items.Count; i++)
        {
            foreach(Item item in items[i].items)
            {
                item.DisableImage();
            }
        }

        currentItems = items[stage].items;
        itemCount = currentItems.Count;

        GetComponent<Image>().sprite = (state == RoomState.Day) ? dayBacking : nightBacking;

        // enable correct assets
        foreach (Item item in currentItems)
        {
            item.SetImage(state);
        }
    }

    public IEnumerator ShowHint()
    {
        foreach(Item item in currentItems)
        {
            if (!item.resolved)
            {
                item.GetComponent<Button>().interactable = false;
            }
        }

        yield return new WaitForSeconds(0.25f);
        
        foreach (Item item in currentItems)
        {
            if (!item.resolved)
            {
                item.GetComponent<Button>().interactable = true;
            }
        }
    }

    public Item GetItem(string id)
    {
        return currentItems.Find(x => x.id == id);
    }
}
