﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    public static RoomManager instance;

    public Transform roomParent;
    public GameObject investigationImage;
    public CloseupView closeupView;

    private Room currentRoom;
    private GameObject gc;

    private int interactedWith = 0;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public static void LoadRoom(Room room, RoomState state, int stage)
    {
        if (instance)
        {
            instance.gc = Instantiate(room.gameObject, instance.roomParent);
            instance.currentRoom = instance.gc.GetComponent<Room>();

            instance.currentRoom.Init(state, stage);
            instance.interactedWith = 0;

            instance.investigationImage.SetActive(true);

            // update mode
            Director.SetState(DirectorState.Investigation);

            CharacterManager.ToggleCharacters(false);
        }
    }

    public static void ShowHint()
    {
        if (instance)
        {
            instance.StartCoroutine(instance.currentRoom.ShowHint());
        }
    }

    public static void CheckDone()
    {
        if (instance)
        {
            if (instance.interactedWith >= instance.currentRoom.itemCount)
            {
                UnloadRoom();
            }
        }
    }

    public static void ShowCloseup(Sprite image, string text)
    {
        if (instance)
        {
            instance.closeupView.Init(image, text);
        }
    }

    public static void BlockInteraction(bool blocked)
    {
        if (instance)
        {
            instance.investigationImage.GetComponent<Image>().raycastTarget = (blocked) ? true : false;
        }
    }

    public static void UnloadRoom()
    {
        instance.investigationImage.SetActive(false);

        Destroy(instance.gc);

        CharacterManager.ToggleCharacters(true);
        Director.SetState(DirectorState.Chapter);
        Director.Continue();
    }

    public static void FindItem(string id)
    {
        Item item = instance.currentRoom.GetItem(id);
        if (item)
        {
           instance.interactedWith += 1;

            item.Resolve();
        }
        else
        {
            Debug.LogWarningFormat("No item with id {0}", id);
        }
    }
}
