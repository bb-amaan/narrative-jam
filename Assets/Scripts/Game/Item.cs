﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public string id;

    public Sprite dayImage;
    public Sprite nightImage;

    public GameObject steps;

    [HideInInspector]
    public Image image;

    public bool resolved = false;

    public void SetImage(RoomState state)
    {
        image = GetComponent<Image>();

        Sprite sprite = (state == RoomState.Day) ? dayImage : nightImage;
        if (sprite)
        {
            image.enabled = true;
            image.sprite = sprite;
        }
        else
        {
            image.enabled = false;
        }
    }

    public void DisableImage()
    {
        image = GetComponent<Image>();
        image.enabled = false;
    }

    public virtual void Resolve()
    {
        CoreScene[] coreScenes = steps.GetComponentsInChildren<CoreScene>();
        List<CoreScene> scenes = new List<CoreScene>();

        foreach (CoreScene coreScene in coreScenes)
        {
            scenes.Add(coreScene);
        }

        // tell the director to play some steps
        Director.PlayInvestigation(scenes);

        Button button = GetComponent<Button>();
        button.interactable = false;

        resolved = true;
    }
}
