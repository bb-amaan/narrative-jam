﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[System.Serializable]

public class Hangman : MonoBehaviour
{
    public List<TextMeshProUGUI> letters;
    public TextMeshProUGUI strikeText;

    private string word = "darkness";
    private int strikes = 0;

    public void Init()
    {
        strikes = 0;
        strikeText.text = "";

        gameObject.SetActive(true);
    }

    public void HandleButtonInput(string input)
    {
        bool guessedCorrect = false;

        for (int i = 0; i < word.Length; i++)
        {
            char c = word[i];
            if (c.ToString().ToLower() == input.ToLower())
            {
                letters[i].text = c.ToString().ToUpper();

                guessedCorrect = true;
            }
        }

        if (!guessedCorrect)
        {
            strikes++;
            strikeText.text += "X";
            if (strikes == 3)
            {
                gameObject.SetActive(false);
                Director.Continue();
            }
        }
        else
        {
            CheckInput();
        }
    }

    void CheckInput()
    {
        string created = "";
        for (int i = 0; i < letters.Count; i++)
        {
            created += letters[i].text;
        }

        if (created.ToLower() == word.ToLower())
        {
            StartCoroutine(CorrectRoutine());
        }
    }

    IEnumerator CorrectRoutine()
    {
        // ew
        for (int i = 0; i < letters.Count; i++)
        {
            letters[i].color = Color.green;
        }

        yield return new WaitForSeconds(0.25f);

        for (int i = 0; i < letters.Count; i++)
        {
            letters[i].color = Color.white;
        }

        yield return new WaitForSeconds(0.25f);

        for (int i = 0; i < letters.Count; i++)
        {
            letters[i].color = Color.green;
        }

        // TODO: correct SFX?

        yield return new WaitForSeconds(0.5f);

        gameObject.SetActive(false);

        Director.instance.stepIndex += 6; // hack to bypass fail
        Director.Continue();
    }
}
