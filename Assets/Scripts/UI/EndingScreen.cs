﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class EndingScreen : MonoBehaviour
{
    public TextMeshProUGUI endingText;

    public void Setup()
    {
        string endingType = PlayerPrefs.GetString("ending");

        string friendlyChapter = UppercaseFirst(endingType);
        int relationship = CharacterManager.GetRelationship(GetCharacterNameFromString(friendlyChapter));

        string ending = string.Format("{0} - {1}", friendlyChapter, (relationship >= 2) ? "Friend" : "Basic");

        endingText.text = string.Format("You got the {0} ending", ending);

        gameObject.SetActive(true);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public CharacterName GetCharacterNameFromString(string name)
    {
        switch (name)
        {
            case "Nora":
                return CharacterName.Nora;
            case "Wyatt":
                return CharacterName.Wyatt;
            case "Jordan":
                return CharacterName.Jordan;
            case "Layton":
                return CharacterName.Layton;
        }

        return CharacterName.None;
    }

    public string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }
}
