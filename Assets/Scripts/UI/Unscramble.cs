﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Unscramble : MonoBehaviour
{
    public GameObject letter;
    public GameObject parent;

    private string targetWord;
    private List<GameObject> letters = new List<GameObject>();

    public void Setup(string word)
    {
        targetWord = word;

        foreach(char c in word)
        {
            GameObject wordLetter = Instantiate(letter);
            wordLetter.GetComponentInChildren<TextMeshProUGUI>().text = c.ToString().ToUpper();

            letters.Add(wordLetter);
        }

        Shuffle(letters);

        foreach(GameObject gc in letters)
        {
            gc.transform.SetParent(parent.transform);
            gc.SetActive(true);
        }

        gameObject.SetActive(true);
    }

    public void CheckAnswer()
    {
        string newWord = "";

        letters.Sort((gc1, gc2) => gc1.transform.position.x.CompareTo(gc2.transform.position.x));

        foreach(GameObject letter in letters)
        {
            TextMeshProUGUI textMeshPro = letter.GetComponentInChildren<TextMeshProUGUI>();
            newWord += textMeshPro.text;
        }

        Debug.LogFormat("Word created {0}", newWord);

        if (newWord.ToLower() == targetWord.ToLower())
        {
            gameObject.SetActive(false);

            foreach (GameObject gc in letters)
            {
                Destroy(gc);
            }

            letters = new List<GameObject>();

            Director.Continue();
        }
    }

    void Shuffle<T>(IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}
