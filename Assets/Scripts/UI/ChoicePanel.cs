﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChoicePanel : MonoBehaviour
{
    public Button choiceButton;
    public Transform parent;

    private List<Button> buttons = new List<Button>();

    public void Setup(List<Choice> choices)
    {
        foreach(Choice choice in choices)
        {
            Button button = Instantiate(choiceButton, parent);
            button.GetComponentInChildren<TextMeshProUGUI>().text = choice.choiceText;

            button.gameObject.SetActive(true);
            button.onClick.AddListener(Handle);

            buttons.Add(button);
        }

        gameObject.SetActive(true);
    }

    public void Handle()
    {
        TextMeshProUGUI text = EventSystem.current.currentSelectedGameObject.GetComponentInChildren<TextMeshProUGUI>();
        if (text)
        {
            ChoiceManager.HandleChoiceInput(text.text);

            Cleanup();
        }
    }

    public void Cleanup()
    {
        foreach(Button button in buttons)
        {
            Destroy(button.gameObject);
        }

        buttons = new List<Button>();
        gameObject.SetActive(false);
    }
}
