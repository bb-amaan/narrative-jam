﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public List<Button> chapterButtons;
    public Button playButton;
    public Button newGameButton;

    public GameObject chapterScreen;
    public GameObject mainScreen;
    public GameObject loadingScreen;
    public GameObject creditsScreen;

    private void Start()
    {
        // do we have some data?
        int chapter = PlayerPrefs.GetInt("chapter", 0);
        if (chapter >= 1)
        {
            playButton.GetComponentInChildren<TextMeshProUGUI>().text = string.Format("START {0}", GetChapterName(chapter).ToUpper());

            newGameButton.gameObject.SetActive(true);
        }
    }

    public string GetChapterName(int index)
    {
        switch(index)
        {
            case 0:
                return "Introduction";
            case 1:
                return "Chapter 1";
            case 2:
                return "Chapter 2";
            case 3:
                return "Chapter 3";
            case 4:
                return "Chapter 4";
            case 5:
                return "Epilogue";
        }

        return "";
    }

    public void ShowChapterScreen()
    {
        mainScreen.SetActive(false);
        creditsScreen.SetActive(false);

        // enable correct buttons based on progress
        int chapter = PlayerPrefs.GetInt("chapter", 0);

        for (int i = 0; i < chapterButtons.Count; i++)
        {
            chapterButtons[i].interactable = false;

            if (chapter >= i)
            {
                chapterButtons[i].interactable = true;
            }
        }

        chapterScreen.SetActive(true);
    }

    public void ShowMainMenu()
    {
        chapterScreen.SetActive(false);
        mainScreen.SetActive(true);
        creditsScreen.SetActive(false);
    }

    public void StartChapter(int index)
    {
        // remove the relationship data
        StartCoroutine(Load(index, true));
    }

    IEnumerator Load(int index, bool manualLoaded = false)
    {
        loadingScreen.SetActive(true);

        AsyncOperation operation = SceneManager.LoadSceneAsync("GameScene", LoadSceneMode.Additive);
        while (!operation.isDone)
        {
            yield return null;
        }

        Director.instance.sceneIndex = index;
        Director.instance.stepIndex = 0;

        GameController.ResetRelationshipDataForChapter(index); // starting from the beginning again

        if (manualLoaded)
            GameController.instance.chapterSelected = true;

        SceneManager.UnloadSceneAsync("MainMenu");

        GameController.Play();
    }

    public void StartNewGame()
    {
        PlayerPrefs.DeleteAll();

        StartCoroutine(Load(0));
    }

    public void ContinueGame()
    {
        StartCoroutine(Load(PlayerPrefs.GetInt("chapter", 0)));
    }

    public void ShowCredits()
    {
        chapterScreen.SetActive(false);
        mainScreen.SetActive(false);
        creditsScreen.SetActive(true);
    }
}
