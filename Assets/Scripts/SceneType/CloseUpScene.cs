﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseUpScene : CoreScene
{
    public Sprite closeupImage;
    public string text;
    public override IEnumerator Play()
    {
        RoomManager.ShowCloseup(closeupImage, text);

        return base.Play();
    }
}
