﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChoiceType
{
    Ignore,
    Positive,
    Negative
}

[System.Serializable]
public class Dialogue
{
    public CharacterName character;
    public Emotion emotion;

    public string dialogue;
    public AudioClip audioClip;
}

[System.Serializable]
public class Choice
{
    public string choiceText;
    public ChoiceType choiceType;

    public CharacterName affected;
    public int amount;

    public List<Dialogue> dialogues;
    public GameObject steps;

    public string endingId;
}

public class MultipleChoiceScene : CoreScene
{
    public List<Choice> choice;

    public override IEnumerator Play()
    {
        // display all the choices
        ChoiceManager.SetupChoiceButtons(choice);

        return base.Play();
    }
}
