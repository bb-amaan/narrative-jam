﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnscrambleScene : CoreScene
{
    public string word = "";

    public override IEnumerator Play()
    {
        GameController.SetupUnscramble(word);

        return base.Play();
    }
}
