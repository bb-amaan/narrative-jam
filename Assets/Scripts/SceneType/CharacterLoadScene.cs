﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterPositionData
{
    public CharacterName name;
    public Emotion emotion;
    public Position position;
}

public class CharacterLoadScene : CoreScene
{
    public List<CharacterPositionData> characters;

    public override IEnumerator Play()
    {
        foreach(CharacterPositionData characterPosition in characters)
        {
            CharacterManager.PlaceInSlot(characterPosition.position, characterPosition.name, characterPosition.emotion);
        }

        yield return new WaitForSeconds(0.5f);

        Director.Continue();
    }
}
