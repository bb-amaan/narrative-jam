﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueLoad
{
    public int relationship;
    public List<Dialogue> dialogues;
}

public class LoadDialogeScene : CoreScene
{
    public CharacterName characterName;
    public List<DialogueLoad> dialogueLoads;

    public override IEnumerator Play()
    {
        int relationship = CharacterManager.GetRelationship(characterName);

        // TODO: tell the user what ending they have
        if (relationship >= dialogueLoads[0].relationship)
        {
            Director.AddSteps(dialogueLoads[0].dialogues);
        }
        else
        {
            if (dialogueLoads[1].dialogues.Count > 0)
            {
                Director.AddSteps(dialogueLoads[1].dialogues);
            }
            else
            {
                Director.Continue();
            }
        }

        return base.Play();
    }
}
