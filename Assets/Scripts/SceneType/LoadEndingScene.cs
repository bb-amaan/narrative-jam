﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ending
{
    public string id;
    public GameObject steps;
}

public class LoadEndingScene : CoreScene
{
    public List<Ending> endings;
    public override IEnumerator Play()
    {
        string ending = PlayerPrefs.GetString("ending");
        if (!string.IsNullOrEmpty(ending))
        {
            Ending chosen = endings.Find(x => x.id == ending);
            Director.AddSteps(chosen.steps);
        }

        return base.Play();
    }
}
