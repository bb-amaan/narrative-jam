﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitScene : CoreScene
{
    public float delayTime;

    public override IEnumerator Play()
    {
        yield return new WaitForSeconds(delayTime);

        Director.Continue();
    }
}
