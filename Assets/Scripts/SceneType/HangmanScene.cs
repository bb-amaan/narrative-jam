﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HangmanScene : CoreScene
{
    public override IEnumerator Play()
    {
        GameController.StartHangman();

        return base.Play();
    }
}
