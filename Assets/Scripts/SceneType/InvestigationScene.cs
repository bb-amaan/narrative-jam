﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvestigationScene : CoreScene
{
    public Room room;
    public RoomState state;
    public int stage;

    public override IEnumerator Play()
    {
        RoomManager.LoadRoom(room, state, stage);

        return base.Play();
    }
}
