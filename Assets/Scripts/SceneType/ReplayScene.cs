﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplayScene : CoreScene
{
    public CoreScene scene;

    public override IEnumerator Play()
    {
        Director.ChangeStepIndex(scene);

        return base.Play();
    }
}
